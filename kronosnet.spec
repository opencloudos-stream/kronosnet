Summary: Multipoint-to-Multipoint VPN daemon
Name: kronosnet
Version: 1.25
Release: 6%{?dist}
License: GPLv2+ and LGPLv2+
URL: https://kronosnet.org
Source0: https://kronosnet.org/releases/%{name}-%{version}.tar.xz

BuildRequires: make gcc libqb-devel
BuildRequires: lksctp-tools-devel nss-devel openssl-devel
BuildRequires: zlib-devel lz4-devel lzo-devel xz-devel bzip2-devel
BuildRequires: libzstd-devel libnl3-devel

%description
Kronosnet, often referred to as knet, is a network abstraction layer
designed for High Availability use cases, where redundancy, security,
fault tolerance and fast fail-over are the core requirements of your application.

%package -n libnozzle1
Summary: Simple userland wrapper around kernel tap devices
License: LGPLv2+

%description -n libnozzle1
 This is an over-engineered commodity library to manage a pool
 of tap devices and provides the basic
 pre-up.d/up.d/down.d/post-down.d infrastructure.

%package -n libnozzle1-devel
Summary: Simple userland wrapper around kernel tap devices (developer files)
License: LGPLv2+
Requires: libnozzle1 = %{version}-%{release}
Requires: pkgconfig

%description -n libnozzle1-devel
 This is an over-engineered commodity library to manage a pool
 of tap devices and provides the basic
 pre-up.d/up.d/down.d/post-down.d infrastructure.

%package -n libknet1
Summary: Kronosnet core switching implementation
License: LGPLv2+

%description -n libknet1
 The whole kronosnet core is implemented in this library.
 Please refer to the not-yet-existing documentation for further
 information.

%package -n libknet1-devel
Summary: Kronosnet core switching implementation (developer files)
License: LGPLv2+
Requires: libknet1 = %{version}-%{release}
Requires: pkgconfig

%description -n libknet1-devel
 The whole kronosnet core is implemented in this library.
 Please refer to the not-yet-existing documentation for further
 information.

%package -n libknet1-crypto-nss-plugin
Summary: Provides libknet1 nss support
License: LGPLv2+
Requires: libknet1 = %{version}-%{release}

%description -n libknet1-crypto-nss-plugin
 Provides NSS crypto support for libknet1.

%package -n libknet1-crypto-openssl-plugin
Summary: Provides libknet1 openssl support
License: LGPLv2+
Requires: libknet1 = %{version}-%{release}

%description -n libknet1-crypto-openssl-plugin
 Provides OpenSSL crypto support for libknet1.

%package -n libknet1-compress-zlib-plugin
Summary: Provides libknet1 zlib support
License: LGPLv2+
Requires: libknet1 = %{version}-%{release}

%description -n libknet1-compress-zlib-plugin
 Provides zlib compression support for libknet1.

%package -n libknet1-compress-lz4-plugin
Summary: Provides libknet1 lz4 and lz4hc support
License: LGPLv2+
Requires: libknet1 = %{version}-%{release}

%description -n libknet1-compress-lz4-plugin
 Provides lz4 and lz4hc compression support for libknet1.

%package -n libknet1-compress-lzo2-plugin
Summary: Provides libknet1 lzo2 support
License: LGPLv2+
Requires: libknet1 = %{version}-%{release}

%description -n libknet1-compress-lzo2-plugin
 Provides lzo2 compression support for libknet1.

%package -n libknet1-compress-lzma-plugin
Summary: Provides libknet1 lzma support
Requires: libknet1 = %{version}-%{release}

%description -n libknet1-compress-lzma-plugin
 Provides lzma compression support for libknet1.

%package -n libknet1-compress-bzip2-plugin
Summary: Provides libknet1 bzip2 support
License: LGPLv2+
Requires: libknet1 = %{version}-%{release}

%description -n libknet1-compress-bzip2-plugin
 Provides bzip2 compression support for libknet1.

%package -n libknet1-compress-zstd-plugin
Summary: Provides libknet1 zstd support
License: LGPLv2+
Requires: libknet1 = %{version}-%{release}

%description -n libknet1-compress-zstd-plugin
 Provides zstd compression support for libknet1.

%package -n libknet1-crypto-plugins-all
Summary: Provides libknet1 crypto plugins meta package
License: LGPLv2+
Requires: libknet1-crypto-nss-plugin = %{version}-%{release}
Requires: libknet1-crypto-openssl-plugin = %{version}-%{release}

%description -n libknet1-crypto-plugins-all
 Provides meta package to install all of libknet1 crypto plugins

%package -n libknet1-compress-plugins-all
Summary: Provides libknet1 compress plugins meta package
License: LGPLv2+
Requires: libknet1-compress-zlib-plugin = %{version}-%{release}
Requires: libknet1-compress-lz4-plugin = %{version}-%{release}
Requires: libknet1-compress-lzo2-plugin = %{version}-%{release}
Requires: libknet1-compress-lzma-plugin = %{version}-%{release}
Requires: libknet1-compress-bzip2-plugin = %{version}-%{release}
Requires: libknet1-compress-zstd-plugin = %{version}-%{release}

%description -n libknet1-compress-plugins-all
 Meta package to install all of libknet1 compress plugins

%package -n libknet1-plugins-all
Summary: Provides libknet1 plugins meta package
License: LGPLv2+
Requires: libknet1-compress-plugins-all = %{version}-%{release}
Requires: libknet1-crypto-plugins-all = %{version}-%{release}

%description -n libknet1-plugins-all
 Meta package to install all of libknet1 plugins

%prep
%autosetup

%build
%configure \
	--disable-install-tests \
	--disable-man \
	--enable-libknet-sctp \
	--enable-crypto-nss \
	--enable-crypto-openssl \
	--enable-compress-zlib \
	--enable-compress-lz4 \
	--enable-compress-lzo2 \
	--enable-compress-lzma \
	--enable-compress-bzip2 \
	--enable-compress-zstd \
	--enable-libnozzle \
	--with-initdefaultdir=%{_sysconfdir}/sysconfig/ \
	--with-systemddir=%{_unitdir}

%make_build

%install
%make_install

find %{buildroot} -name "*.a" -exec rm {} \;
find %{buildroot} -name "*.la" -exec rm {} \;

rm -rf %{buildroot}/usr/share/doc/kronosnet

%files -n libnozzle1
%license COPYING.* COPYRIGHT
%{_libdir}/libnozzle.so.*

%files -n libnozzle1-devel
%license COPYING.* COPYRIGHT
%{_libdir}/libnozzle.so
%{_includedir}/libnozzle.h
%{_libdir}/pkgconfig/libnozzle.pc

%files -n libknet1
%license COPYING.* COPYRIGHT
%{_libdir}/libknet.so.*
%dir %{_libdir}/kronosnet

%files -n libknet1-devel
%license COPYING.* COPYRIGHT
%{_libdir}/libknet.so
%{_includedir}/libknet.h
%{_libdir}/pkgconfig/libknet.pc

%files -n libknet1-crypto-nss-plugin
%{_libdir}/kronosnet/crypto_nss.so

%files -n libknet1-crypto-openssl-plugin
%{_libdir}/kronosnet/crypto_openssl.so

%files -n libknet1-compress-zlib-plugin
%{_libdir}/kronosnet/compress_zlib.so

%files -n libknet1-compress-lz4-plugin
%{_libdir}/kronosnet/compress_lz4.so
%{_libdir}/kronosnet/compress_lz4hc.so

%files -n libknet1-compress-lzo2-plugin
%{_libdir}/kronosnet/compress_lzo2.so

%files -n libknet1-compress-lzma-plugin
%{_libdir}/kronosnet/compress_lzma.so

%files -n libknet1-compress-bzip2-plugin
%{_libdir}/kronosnet/compress_bzip2.so

%files -n libknet1-compress-zstd-plugin
%{_libdir}/kronosnet/compress_zstd.so

%files -n libknet1-crypto-plugins-all

%files -n libknet1-compress-plugins-all

%files -n libknet1-plugins-all

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.25-6
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.25-5
- Rebuilt for loongarch release

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.25-4
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.25-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.25-2
- Rebuilt for OpenCloudOS Stream 23

* Tue Feb 07 2023 cunshunxia <cunshunxia@tencent.com> - 1.25-1
- initial build
